package com.example.autoHub.student;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
@Service
public class StudentService {

    @GetMapping()
    public List<Student> getStudent(){
        return List.of(
                new Student(
                        1L,
                        "Sandile",
                        22,
                        LocalDate.of(2001, Month.APRIL,23),
                        "sanndlovu021@student.wethinkcode.co.za"

                )
        );
    }
}

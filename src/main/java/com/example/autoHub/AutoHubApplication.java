package com.example.autoHub;

import com.example.autoHub.student.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@SpringBootApplication
public class AutoHubApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoHubApplication.class, args);

	}

	@GetMapping()
	public List<Student> getStudent(){
		return List.of(
				new Student(
						1L,
						"Sandile",
						22,
						LocalDate.of(2001, Month.APRIL,23),
						"sanndlovu021@student.wethinkcode.co.za"

				)
		);
	}

}
